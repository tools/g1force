use aes_gcm::{aead::AeadMutInPlace, KeyInit};
use rayon::iter::{IntoParallelIterator, ParallelIterator};

pub fn run() {
	let iv: &[u8] = &[
		127, 126, 63, 250, 202, 36, 9, 248, 210, 125, 100, 13, 49, 167, 186, 139,
	];
	let ciphertext: &[u8] = &[
		174, 154, 188, 188, 193, 232, 182, 223, 245, 68, 216, 160, 133, 127, 178, 123, 58, 81, 186,
		35, 180, 90, 153, 223, 210, 54, 127, 86, 218, 236, 144, 65, 32, 175, 146, 80, 171, 92, 165,
		165, 139, 146, 139, 16, 102, 200, 189, 140, 175, 29, 2, 109, 196, 225, 0, 156, 158, 214,
		154, 62, 219, 172, 75, 135, 51, 208, 26, 92, 156, 51, 153, 186, 73, 251, 127, 47, 24, 78,
		8, 0, 210, 227, 112, 196, 150, 107, 127, 151, 8, 117,
	];

	// let kdf = argon2::Argon2::new(
	// 	argon2::Algorithm::Argon2i,
	// 	argon2::Version::V0x10,
	// 	argon2::Params::new(65536, 2, 1, Some(32)).unwrap(),
	// );
	// let mut out = [0; 32];
	// kdf.hash_password_into(b"pass123456", b"somesalt", &mut out)
	// 	.unwrap();
	// println!("{:?}", &out);

	let kdf = argon2::Argon2::new(
		argon2::Algorithm::Argon2id,
		argon2::Version::V0x13,
		argon2::Params::new(65536, 3, 4, Some(32)).unwrap(),
	);
	let nonce = aes_gcm::Nonce::from_slice(iv);
	(1230..1235)
		.into_par_iter()
		.map(|i| format!("{:04}", i))
		.for_each(|pin| {
			println!("> {pin}");
			let mut buffer =
				aes_gcm::aead::heapless::Vec::<u8, 512>::from_slice(ciphertext).unwrap();
			let mut key = [0; 32];
			kdf.hash_password_into(pin.as_bytes(), iv, &mut key)
				.unwrap();
			if dbg!(
				aes_gcm::AesGcm::<aes_gcm::aes::Aes256, typenum::U16>::new_from_slice(&key)
					.unwrap()
					.decrypt_in_place(&nonce, &[], &mut buffer)
			)
			.is_ok()
			{
				println!("{pin}: {buffer:?}");
			}
		});
}
