use crate::LegacySubcommand;

use bruteforce::{charset::Charset, BruteForce};
use dup_crypto::{
	bases::b58::ToBase58,
	keys::{
		ed25519::{KeyPairFromSaltedPasswordGenerator, PublicKey, SaltedPassword},
		KeyPair, PublicKey as PublicKeyTrait,
	},
};
use indicatif::{ProgressBar, ProgressStyle};
use rayon::prelude::*;
use regex::Regex;
use std::{
	collections::HashMap,
	io::{stdin, BufRead, BufReader},
	mem::MaybeUninit,
	sync::RwLock,
};

const CHARSET: Charset = Charset::new(&[
	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
	't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
	'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4',
	'5', '6', '7', '8', '9', '_', '.', '-', '!', '@', '*', '$', '?', '&', '%', ' ',
]);

struct PubkeyMetadata {
	amount: u32,
	member: bool,
}

enum InputData {
	Pubkey(PublicKey),
	PubkeyList(Vec<PublicKey>),
	PubkeyListWithMetadata(HashMap<PublicKey, PubkeyMetadata>),
	Regex(Regex),
}

impl InputData {
	fn new(opt: &LegacySubcommand) -> Self {
		if let Some(ref pubkey) = opt.pubkey {
			if opt.regex {
				Self::Regex(Regex::new(pubkey).expect("Invalid regex"))
			} else {
				Self::Pubkey(PublicKey::from_base58(pubkey).expect("Invalid pubkey"))
			}
		} else if opt.metadata {
			let mut pubkeys = HashMap::<PublicKey, PubkeyMetadata>::new();

			for (i, raw_line) in BufReader::new(stdin()).lines().enumerate() {
				let raw_line = raw_line.expect("Error reading CSV");
				let mut rows = raw_line.split(&[' ', '\t', ',', ';'][..]);
				let pubkey = PublicKey::from_base58(
					rows.next()
						.unwrap_or_else(|| panic!("Missing pubkey line {}", i)),
				)
				.unwrap_or_else(|_| panic!("Invalid pubkey line {}", i));
				pubkeys.insert(
					pubkey,
					PubkeyMetadata {
						amount: rows
							.next()
							.unwrap_or_else(|| panic!("Missing amount line {}", i))
							.parse()
							.unwrap_or_else(|_| panic!("Invalid amount line {}", i)),
						member: rows
							.next()
							.unwrap_or_else(|| panic!("Missing member line {}", i))
							== "1",
					},
				);
			}
			Self::PubkeyListWithMetadata(pubkeys)
		} else {
			let mut pubkeys = Vec::<PublicKey>::new();

			for (i, raw_line) in BufReader::new(stdin()).lines().enumerate() {
				let raw_line = raw_line.expect("Error reading CSV");
				let pubkey = PublicKey::from_base58(
					raw_line
						.split_once(&[' ', '\t', ',', ';'][..])
						.unwrap_or_else(|| panic!("Missing pubkey line {}", i))
						.0,
				)
				.unwrap_or_else(|_| panic!("Invalid pubkey line {}", i));
				pubkeys.push(pubkey);
			}
			Self::PubkeyList(pubkeys)
		}
	}
}

struct Tester<'a> {
	pubkey: MaybeUninit<&'a PublicKey>,
	pubkey_list: MaybeUninit<&'a Vec<PublicKey>>,
	pubkey_list_with_metadata: MaybeUninit<&'a HashMap<PublicKey, PubkeyMetadata>>,
	regex: MaybeUninit<&'a Regex>,

	/// Returns (match, stop)
	test: fn(&Self, &PublicKey) -> (bool, bool),
}

impl<'a> Tester<'a> {
	fn new(data: &'a InputData) -> Self {
		match data {
			InputData::Pubkey(pubkey) => Self {
				pubkey: MaybeUninit::new(pubkey),
				pubkey_list: MaybeUninit::uninit(),
				pubkey_list_with_metadata: MaybeUninit::uninit(),
				regex: MaybeUninit::uninit(),
				test: Self::test_pubkey,
			},
			InputData::PubkeyList(pubkey_list) => Self {
				pubkey: MaybeUninit::uninit(),
				pubkey_list: MaybeUninit::new(pubkey_list),
				pubkey_list_with_metadata: MaybeUninit::uninit(),
				regex: MaybeUninit::uninit(),
				test: Self::test_pubkey_list,
			},
			InputData::PubkeyListWithMetadata(pubkey_list_with_metadata) => Self {
				pubkey: MaybeUninit::uninit(),
				pubkey_list: MaybeUninit::uninit(),
				pubkey_list_with_metadata: MaybeUninit::new(pubkey_list_with_metadata),
				regex: MaybeUninit::uninit(),
				test: Self::test_pubkey_list_with_metadata,
			},
			InputData::Regex(regex) => Self {
				pubkey: MaybeUninit::uninit(),
				pubkey_list: MaybeUninit::uninit(),
				pubkey_list_with_metadata: MaybeUninit::uninit(),
				regex: MaybeUninit::new(regex),
				test: Self::test_regex,
			},
		}
	}

	fn test_pubkey(&self, x: &PublicKey) -> (bool, bool) {
		(x == unsafe { self.pubkey.assume_init() }, true)
	}

	fn test_pubkey_list(&self, x: &PublicKey) -> (bool, bool) {
		(unsafe { self.pubkey_list.assume_init() }.contains(x), false)
	}

	fn test_pubkey_list_with_metadata(&self, x: &PublicKey) -> (bool, bool) {
		(
			unsafe { self.pubkey_list_with_metadata.assume_init() }.contains_key(x),
			false,
		)
	}

	fn test_regex(&self, x: &PublicKey) -> (bool, bool) {
		(
			unsafe { self.regex.assume_init() }.is_match(&x.to_base58()),
			false,
		)
	}
}

pub fn run(opt: LegacySubcommand) {
	let data = InputData::new(&opt);

	let tester = Tester::new(&data);

	let generator = KeyPairFromSaltedPasswordGenerator::with_default_parameters();

	let progress_bar = ProgressBar::new(opt.ntests as u64);
	progress_bar.set_style(
		ProgressStyle::default_bar()
			.template("{elapsed_precise}  {eta}  {per_sec}  {pos}/{len}  {percent}% {wide_bar}")
			.unwrap(),
	);
	progress_bar.tick();

	let found = RwLock::new(Vec::<(PublicKey, String, String)>::new());
	let continue_test = RwLock::new(true);

	BruteForce::new(CHARSET)
		.enumerate()
		.map_while(|(i, s1)| (*continue_test.read().unwrap() && i < opt.ntests).then_some(s1))
		.par_bridge()
		.for_each(|s1| {
			for (j, s2) in BruteForce::new(CHARSET).enumerate() {
				let pubkey = generator
					.generate(SaltedPassword::new(s1.clone(), s2.clone()))
					.public_key();
				let (res_match, res_stop) = (tester.test)(&tester, &pubkey);
				if res_match {
					let mut found = found.write().unwrap();
					found.push((pubkey, s1.clone(), s2));
				}
				if res_stop {
					let mut continue_test = continue_test.write().unwrap();
					*continue_test = true;
				}
				if j >= opt.ntests {
					break;
				}
			}
			progress_bar.inc(1u64);
		});

	let found = found.into_inner().unwrap();

	match data {
		InputData::PubkeyListWithMetadata(pubkey_list_with_metadata) => {
			let mut total_amount = 0u32;
			let mut total_member = 0u32;

			for (pubkey, s1, s2) in found {
				let metadata = pubkey_list_with_metadata.get(&pubkey).unwrap();
				total_amount += metadata.amount;
				if metadata.member {
					total_member += 1;
				}
				println!(
					"{}\t{}\t{}\t{}\t{}",
					pubkey.to_base58(),
					s1,
					s2,
					metadata.amount,
					metadata.member as u8,
				);
			}

			eprintln!("Total amount: {}", total_amount);
			eprintln!("Total member: {}", total_member);
		}
		_ => {
			for (pubkey, s1, s2) in found {
				println!("{}\t{}\t{}", pubkey.to_base58(), s1, s2);
			}
		}
	}
	eprintln!();
}
