mod modes;

use clap::{Parser, Subcommand};

#[derive(Clone, Debug, Parser)]
#[clap(name = "g1force")]
pub struct MainOpt {
	#[command(subcommand)]
	mode: Mode,
}

#[derive(Clone, Debug, Subcommand)]
pub enum Mode {
	Gazelle(GazelleSubcommand),
	Legacy(LegacySubcommand),
}

#[derive(Clone, Debug, Parser)]
pub struct LegacySubcommand {
	/// Use metadata in CSV (amount & member)
	#[clap(short, long)]
	metadata: bool,

	/// Number of iterations
	#[clap(short, long)]
	ntests: usize,

	/// Use regex (Perl-style)
	#[clap(short, long)]
	regex: bool,

	/// Wanted pubkey or regex. If empty, reads stdin.
	#[clap(short, long)]
	pubkey: Option<String>,
}

#[derive(Clone, Debug, Parser)]
pub struct GazelleSubcommand {}

fn main() {
	let opt = MainOpt::parse();
	match opt.mode {
		Mode::Legacy(subcommand) => modes::legacy::run(subcommand),
		Mode::Gazelle(_) => modes::gazelle::run(),
	}
}
