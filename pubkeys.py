#!/usr/bin/env python3
# GNU AGPL v3, CopyLeft 2020 Pascal Engélibert

import os, sys, re, json, plyvel

def getargv(arg, default=""):
	if arg in sys.argv and len(sys.argv) > sys.argv.index(arg)+1:
		return sys.argv[sys.argv.index(arg)+1]
	else:
		return default

if __name__ == "__main__":
	dbpath = os.path.expanduser(getargv("-d", "~/.config/duniter/duniter_default/data/leveldb"))
	
	pubkeys = {}
	
	windex = plyvel.DB(dbpath+"/level_wallet/")
	for k, v in windex:
		pubkey = re.search("^SIG\(([a-zA-Z0-9]{43,44})\)$", k.decode())
		if pubkey:
			balance = json.loads(v.decode())["balance"]
			if balance != 0:
				pubkeys[pubkey[1]] = [balance, False]
	
	iindex = plyvel.DB(dbpath+"/level_iindex/")
	for k, v in iindex:
		for i in json.loads(v.decode()):
			if i["member"]:
				pubkey = k.decode()
				if pubkey in pubkeys:
					pubkeys[pubkey][1] = True
				else:
					pubkeys[pubkey] = [0, True]
				break
	
	for pubkey in pubkeys:
		print("{}\t{}\t{}".format(pubkey, pubkeys[pubkey][0], int(pubkeys[pubkey][1])))
