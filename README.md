# Ğ1force

Scrypt bruteforce for Duniter currencies

## Build

You need to switch to Rust Nightly first.

    cargo build --release

## Use

`plyvel` is needed for Python script.

    python3 pubkeys.py [-d duniter_leveldb_path] > pubkeys.csv
    cat pubkeys.csv | ./target/release/g1force -n <N>

Where `<N>` is the sqrt of the total number of trials.

`stdout` is CSV (tab separated):

    pubkey amount member

Please don't be a dick: warn people before stealing them. If you do steal, please send the money to crowdfunding accounts.

You can also bruteforce a specific pubkey:

    ./target/release/g1force -n <N> -w <pubkey>

## Future

Dictionary attack should be implemented.

## License

GNU AGPL v3, CopyLeft 2020-2021 Pascal Engélibert
